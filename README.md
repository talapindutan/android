# ᜆᜎᜉᜒᜈ᜕ᜇᜓᜆᜈ᜕

Powered by Openboard

100% FOSS keyboard, based on AOSP.

## ᜉᜄ᜕ᜊᜊᜄᜓ

-   ᜉᜄ᜕ᜆᜅ᜕ᜄᜎ᜕ ᜈᜅ᜕ ᜋᜅ ᜑᜒᜈ᜕ᜇᜒ ᜄᜒᜈᜄᜋᜒᜆ᜕ ᜈ ᜋᜅ resource ᜀᜆ᜕ ᜏᜒᜃ ᜈ ᜏᜎᜅ᜕ ᜃᜂᜄ᜕ᜈᜌᜈ᜕ ᜐ ᜉᜒᜎᜒᜉᜒᜈᜐ᜕
-   ᜉᜄ᜕ᜇᜄ᜕ᜇᜄ᜕ ᜈᜅ᜕ ᜊᜓᜑᜒᜇ᜕᜵ ᜑᜈᜓᜈᜓᜂ᜵ ᜊᜌ᜕ᜊᜌᜒᜈ᜕᜵ ᜆᜄ᜕ᜊᜈ᜕ᜏ ᜀᜆ᜕ ᜊᜇ᜕ᜎᜒᜆ᜕ ᜈ keyboard layout
-   ᜉᜄ᜕ᜇᜄ᜕ᜇᜄ᜕ ᜈᜅ᜕ ᜉᜒᜎᜒᜉᜒᜈᜓ ᜈ ᜆᜎᜐᜎᜒᜆᜀᜈ᜕
-   ᜉᜄ᜕ᜊᜄᜓ ᜐ icon ᜀᜆ᜕ title

### ᜉᜄ᜕ᜇᜏᜈ᜕ᜎᜓᜇ᜕

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.mawadika.talapindutan.inputmethod.latin)

## ᜎᜒᜐ᜕ᜌᜒᜈ᜕ᜐ᜕ᜌ

The software is licensed under GPLv3

## ᜉᜄ᜕ᜊᜒᜊᜒᜄᜌ᜕ ᜉᜓᜄᜌ᜕

-   OpenBoard by [Openboard Team] for the base code [Openboard](https://github.com/openboard-team/openboard)
-   Icon by [Marco TLS](https://www.marcotls.eu)
-   [AOSP Keyboard](https://android.googlesource.com/platform/packages/inputmethods/LatinIME/)
-   [LineageOS](https://review.lineageos.org/admin/repos/LineageOS/android_packages_inputmethods_LatinIME)
-   [Simple Keyboard](https://github.com/rkkr/simple-keyboard)
-   [Indic Keyboard](https://gitlab.com/indicproject/indic-keyboard)
-   [Contributors](https://github.com/openboard-team/openboard/graphs/contributors)
